//
//  GameBoard.swift
//  Space InvadAR
//
//  Created by Isaac Kim on 5/2/20.
//  Copyright © 2020 Space InvadARs. All rights reserved.
//

import Foundation
import ARKit

class GameBoard {
    var planeAnchor: ARPlaneAnchor?
    var planeNode: SCNNode?
    
    var rBottomCorner: SIMD3<Float>?
    var lBottomCorner: SIMD3<Float>?
    var rTopCorner: SIMD3<Float>?
    var lTopCorner: SIMD3<Float>?
    var center: SIMD3<Float>?
    
    init() {
        
        
    }
    
    //func updateGameBoard(firstResult: ARHitTestResult) {
        
    //}
    
    func createPlane(firstResult: ARHitTestResult, globalNode: SCNNode) {
        self.planeAnchor = firstResult.anchor as? ARPlaneAnchor
        
        // 1 CREATE ANCHOR
        let width = Float(planeAnchor!.extent.x)
        let height = Float(planeAnchor!.extent.z)
        
        self.center = self.planeAnchor!.center
        self.rBottomCorner = self.center! -  SIMD3<Float>(x: width, y: 0, z: -height)
        self.lBottomCorner = self.center! -  SIMD3<Float>(x: -width, y: 0, z: -height)
        self.rTopCorner = self.center! -  SIMD3<Float>(x: width, y: 0, z: height)
        self.lTopCorner = self.center! -  SIMD3<Float>(x: -width, y: 0, z: height)
        
                 
        // 2 CREATE NODE
        let plane = SCNPlane(width: CGFloat(width), height: CGFloat(height))
        
        // 3
        plane.materials.first?.diffuse.contents = UIColor.blue
        
        // 4
        self.planeNode = SCNNode(geometry: plane)
        
        // 5
        let x = CGFloat(planeAnchor!.center.x)
        let y = CGFloat(planeAnchor!.center.y)
        let z = CGFloat(planeAnchor!.center.z)
        planeNode!.position = SCNVector3(x,y,z)
        planeNode!.eulerAngles.x = -.pi / 2
        
        // 6
        globalNode.addChildNode(planeNode!)
        
    
    }
    
    func getShipInitialLocation() -> SIMD3<Float> {
        return lBottomCorner! + SIMD3<Float>(x: planeAnchor!.extent.x / 2.0, y: 0, z: 0)
    }
    
    
}
